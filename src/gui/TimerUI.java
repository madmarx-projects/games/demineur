package gui;

import utils.Settings;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class TimerUI extends JPanel {

    private static int PANEL_WIDTH = 100;

    private JLabel timeLabel;

    public TimerUI() {
        this.setBounds(Settings.GAP + Settings.CASE_NUMBER_WIDTH * Settings.CASE_SIZE - PANEL_WIDTH, Settings.GAP,
                PANEL_WIDTH, Settings.TOP_BAR_SIZE);
        this.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));

        this.timeLabel = new JLabel("00:00");
        this.timeLabel.setFont(new Font("Dialog.bold", Font.PLAIN, 20));
        this.timeLabel.setForeground(Color.GRAY);

        this.add(timeLabel);
    }

    public void updateTime(String time) {
        this.timeLabel.setText(time);
    }

}
