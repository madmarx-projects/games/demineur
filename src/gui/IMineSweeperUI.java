package gui;

public interface IMineSweeperUI {

    /**
     * Révèle la case aux coordonnées lig;col dans le champ de mines
     * 
     * @param lig
     * @param col
     * @param state
     *              String qui permet de connaitre l'état de la case à dévoiler
     */
    void unveilUI(int lig, int col, String state);

    /**
     * Initialise la grille au niveau graphique et ajoute des listeners sur
     * chaque case
     * 
     * @param nbMines
     */
    void initArrayUI(int nbMines);

    /**
     * Informe l'interface graphique que la partie est terminée
     * 
     * @param win
     *            boolean
     */
    void endGame(boolean win);
}
