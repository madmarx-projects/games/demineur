package gui;

import controller.MineSweeperController;
import utils.Settings;

import javax.swing.*;
import java.awt.*;

public class LandmineUI extends JPanel {

    private MineSweeperController controller;

    private CaseUI[][] landmine;
    private int        gameHeight;
    private int        gameWidth;

    public LandmineUI(int gameHeight, int gameWidth) {
        this.gameHeight = gameHeight;
        this.gameWidth = gameWidth;

        this.setLayout(new GridLayout(gameHeight, gameWidth, 1, 1));
        this.setOpaque(false);

        this.setBounds(Settings.GAP, Settings.GAP * 2 + Settings.TOP_BAR_SIZE, gameWidth * Settings.CASE_SIZE,
                gameHeight * Settings.CASE_SIZE);

        this.setVisible(false);
    }

    public void initArray() {
        this.removeAll();

        this.landmine = new CaseUI[this.gameHeight][this.gameWidth];

        for (int i = 0; i < this.gameHeight; ++i) {
            for (int j = 0; j < this.gameWidth; ++j) {
                this.landmine[i][j] = new CaseUI(i, j, this.controller);
                this.add(this.landmine[i][j]);
            }
        }

        this.setVisible(true);
    }

    public void unveil(int lig, int col, String state) {
        landmine[lig][col].unveil(state);
    }

    public void setController(MineSweeperController controller) {
        this.controller = controller;
    }

}
