package gui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import controller.MineSweeperController;
import observer.Observer;
import utils.Settings;

public class MineSweeperUI implements IMineSweeperUI, Observer {

    private final JFrame frame;
    private final JPanel panel;

    private final LandmineUI  landMineUI;
    private final TitleUI     titleUI;
    private final MinesLeftUI minesLeftUI;
    private final TimerUI     timerUI;

    private MineSweeperController controller;

    public MineSweeperUI(int gameHeight, int gameWith) {
        // forcer un minimum de cases
        if (gameHeight < 5) {
            gameHeight = 5;
        }
        if (gameWith < 5) {
            gameWith = 5;
        }

        this.frame = new JFrame(Settings.STR_GAME_TITLE + " 1337");
        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.setResizable(false);
        this.frame.setLayout(null);
        this.frame.setPreferredSize(new Dimension(
                gameWith * Settings.CASE_SIZE + Settings.GAP * 2 + Settings.CASE_SIZE / 2,
                Settings.GAP + Settings.TOP_BAR_SIZE + Settings.GAP + gameHeight * Settings.CASE_SIZE + Settings.GAP));

        this.panel = new JPanel();
        this.panel.setOpaque(true);
        this.panel.setBackground(Color.BLACK);
        this.frame.add(this.panel);

        this.landMineUI = new LandmineUI(gameHeight, gameWith);
        this.titleUI = new TitleUI(Settings.STR_GAME_TITLE);
        this.minesLeftUI = new MinesLeftUI();
        this.timerUI = new TimerUI();

        this.frame.add(this.landMineUI);
        this.frame.add(this.titleUI);
        this.frame.add(this.minesLeftUI);
        this.frame.add(this.timerUI);

        this.frame.pack();
        this.frame.setLocationRelativeTo(null); // la fenêtre se centre au milieu de l'écran
        this.frame.setVisible(true);
    }

    // MINESWEEPERUI
    @Override
    public void unveilUI(final int lig, final int col, final String state) {
        this.landMineUI.unveil(lig, col, state);
    }

    @Override
    public void initArrayUI(final int nbMines) {
        this.titleUI.setText(Settings.STR_GAME_TITLE);
        this.titleUI.setForeground(Color.GRAY);
        this.titleUI.removeRestartListener();

        this.timerUI.updateTime("00:00");

        this.minesLeftUI.setMinesLeft(nbMines);

        this.landMineUI.initArray();
    }

    @Override
    public void endGame(final boolean win) {
        this.titleUI.addRestartListener(this.controller);

        if (win) {
            this.titleUI.setText(Settings.STR_VICTORY);
            this.titleUI.setForeground(Settings.COLORS[0]);
        } else {
            this.titleUI.setText(Settings.STR_DEFEAT);
            this.titleUI.setForeground(Settings.BG_MINED);
        }
    }

    // OBSERVER
    @Override
    public void updateTimer() {
        this.timerUI.updateTime(this.controller.getTime());
    }

    @Override
    public void updateRemainingMines(final int mines) {
        this.minesLeftUI.setMinesLeft(mines);
    }

    public void setController(final MineSweeperController controller) {
        this.controller = controller;

        this.landMineUI.setController(controller);
    }
}
