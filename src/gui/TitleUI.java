package gui;

import controller.MineSweeperController;
import utils.Settings;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TitleUI extends JLabel {

    private static int PANEL_WIDTH = 200;

    private MouseAdapter listener;

    public TitleUI(String title) {
        super(title, SwingConstants.CENTER);
        this.setBounds((Settings.GAP * 2 + Settings.CASE_NUMBER_WIDTH * Settings.CASE_SIZE) / 2 - PANEL_WIDTH / 2,
                Settings.GAP, PANEL_WIDTH, Settings.TOP_BAR_SIZE);
        this.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));

        this.setFont(new Font("Dialog.bold", Font.PLAIN, 15));
    }

    public void addRestartListener(MineSweeperController controller) {
        this.listener = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);

                if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
                    System.out.println("RESTART");
                    controller.restartGame();
                }
            }
        };

        this.addMouseListener(listener);
    }

    public void removeRestartListener() {
        try {
            this.removeMouseListener(listener);
        }
        catch (Exception e) {
            System.err.println("IMPOSSIBLE");
        }
    }

}
