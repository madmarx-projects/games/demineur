package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import controller.MineSweeperController;
import utils.Settings;

public class CaseUI extends JLabel {

    private final MouseListener listener;
    private String              prevState;

    public CaseUI(final int lig, final int col, final MineSweeperController controller) {
        super("", SwingConstants.CENTER);

        this.setPreferredSize(new Dimension(Settings.CASE_SIZE, Settings.CASE_SIZE));
        this.setOpaque(true);
        this.setBackground(Color.WHITE);
        this.setFont(Settings.CASE_FONT);
        // this.setMargin(new Insets(0, 0, 0, 0)); // juste pour voir le texte
        // dans le bouton
        this.setBorder(new LineBorder(Color.WHITE, 1));

        this.listener = new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);

                if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
                    controller.click(lig, col, Settings.LEFT_CLICK_EVENT);
                } else if (SwingUtilities.isRightMouseButton(mouseEvent)) {
                    controller.click(lig, col, Settings.RIGHT_CLICK_EVENT);
                }
            }
        };

        this.addMouseListener(this.listener);
    }

    public void unveil(final String state) {
        this.setBackground(Settings.BG_UNVEILED);

        String str;
        switch (state) {
            case "covered":
                str = Settings.COVERED;
                this.setBackground(Settings.BG_COVERED);
                break;

            case "flag":
                if (this.getText().equals(Settings.FLAGGED)) {
                    this.setBackground(Settings.BG_COVERED);
                    this.unveil(this.prevState);
                }
                str = Settings.FLAGGED;
                break;

            case "mine":
                this.setBackground(Settings.BG_MINED);
                this.setForeground(Settings.FG_MINED);
                str = Settings.MINED;
                break;

            case "flagEndGame":
                this.setBackground(Settings.BG_FLAGGED_ENDGAME);
                this.setForeground(Settings.FG_FLAGGED_ENDGAME);
                str = Settings.MINED;
                break;

            case "mineEndGame":
                this.setBackground(Settings.BG_MINED_ENDGAME);
                this.setForeground(Settings.FG_MINED_ENDGAME);
                str = Settings.MINED;
                break;

            case "coveredMineEndGame":
                this.setBackground(Settings.BG_FLAGGED_ENDGAME);
                this.setForeground(Settings.FG_FLAGGED_ENDGAME);
                str = Settings.MINED;
                break;

            default:
                str = " ";
                try {
                    if (Integer.valueOf(state) > 0) {
                        str = state;
                    }
                }
                catch (final NumberFormatException e) {}
                break;
        }

        this.prevState = this.getText();
        this.setText(str);

        if (state.equals("flag")) {
            this.setForeground(Settings.FG_FLAGGED);
            this.setBackground(Settings.BG_FLAGGED);
        } else {
            try {
                this.setForeground(Settings.COLORS[Integer.valueOf(state) - 1]);
            }
            catch (final Exception e) {
                // could be not a number
            }
        }

        // Dans un souci d'économies de resources, décommenter une fois que le state pattern sera bien testé
        // this.removeMouseListener(this.listener);
    }

}
