package gui;

import utils.Settings;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class MinesLeftUI extends JPanel {

    private static int PANEL_WIDTH = 100;

    private JLabel minesLeftLabel;
    private int    minesLeft;

    public MinesLeftUI() {
        this.setBounds(Settings.GAP, Settings.GAP, PANEL_WIDTH, Settings.TOP_BAR_SIZE);
        this.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));

        this.minesLeftLabel = new JLabel(Integer.toString(this.minesLeft));
        this.minesLeftLabel.setFont(new Font("Dialog.bold", Font.PLAIN, 20));
        this.minesLeftLabel.setForeground(Color.GRAY);

        this.add(minesLeftLabel);
    }

    public void decrementMines() {
        --this.minesLeft;
        updateLabel();
    }

    public void incrementMines() {
        ++this.minesLeft;
        updateLabel();
    }

    private void updateLabel() {
        this.minesLeftLabel.setText(Integer.toString(this.minesLeft));
    }

    public void setMinesLeft(int mines) {
        this.minesLeft = mines;
        updateLabel();
    }

}
