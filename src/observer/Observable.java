package observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {

    private List<Observer> observers = new ArrayList<>();

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void notifyRemainingMines(int remainingMines) {
        for (Observer observer : observers) {
            observer.updateRemainingMines(remainingMines);
        }
    }

    public void notifyAllObserversTimer() {
        for (Observer observer : observers) {
            observer.updateTimer();
        }
    }

}
