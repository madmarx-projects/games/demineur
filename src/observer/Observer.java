package observer;

public interface Observer {

    void updateTimer();

    void updateRemainingMines(int remainingMines);
}
