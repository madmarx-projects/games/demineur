package utils;

import java.awt.Color;
import java.awt.Font;

public abstract class Settings {

    // GAME
    public static final int DIFFICULTY_LVL = 3; // 1-10

    // GRAPHICS' DIMENSIONS
    public static final int CASE_NUMBER_HEIGHT = 20;
    public static final int CASE_NUMBER_WIDTH  = 30;
    public static final int CASE_SIZE          = 25;
    public static final int GAP                = 5;
    public static final int TOP_BAR_SIZE       = 35;

    // STRINGS
    public static final String STR_GAME_TITLE = "Démineur";
    public static final String STR_VICTORY    = "VICTORY !";
    public static final String STR_DEFEAT     = "Lost... Replay?";

    // CASE CONTENT
    public static final Font   CASE_FONT = new Font("Dialog.bold", Font.PLAIN, CASE_SIZE / 2);
    public static final String COVERED   = " ";
    public static final String MINED     = "*";
    public static final String FLAGGED   = "?";

    // EVENTS
    public static final String LEFT_CLICK_EVENT  = "leftClick";
    public static final String RIGHT_CLICK_EVENT = "rightClick";

    // COLORS
    public static final Color BG_UNVEILED        = new Color(230, 230, 230);
    public static final Color BG_COVERED         = new Color(255, 255, 255);
    public static final Color BG_FLAGGED         = new Color(150, 215, 230);
    public static final Color BG_FLAGGED_ENDGAME = new Color(160, 230, 160);
    public static final Color BG_MINED           = new Color(230, 150, 150);
    public static final Color BG_MINED_ENDGAME   = new Color(230, 160, 150);

    public static final Color FG_UNVEILED        = new Color(230, 230, 230);
    public static final Color FG_COVERED         = new Color(230, 230, 230);
    public static final Color FG_FLAGGED         = new Color(25, 145, 230);
    public static final Color FG_FLAGGED_ENDGAME = new Color(25, 145, 230);
    public static final Color FG_MINED           = new Color(230, 20, 20);
    public static final Color FG_MINED_ENDGAME   = new Color(230, 100, 100);

    public static final Color[] COLORS = {
            new Color(156, 206, 0), // color level 1 danger
            new Color(210, 197, 0), // color level 2 danger
            new Color(213, 136, 0), // ...
            new Color(217, 74, 0), // ...
            new Color(225, 0, 58), // ...
            new Color(225, 0, 58), // ...
            new Color(225, 0, 58) // ...
    };

}
