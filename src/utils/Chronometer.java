package utils;

import model.MineSweeper;

import java.util.Timer;

public class Chronometer {

    private MineSweeper mineSweeper;

    private Timer timer;
    private int   m;
    private int   s;

    public Chronometer(MineSweeper mineSweeper) {
        this.mineSweeper = mineSweeper;
        this.start();
    }

    public void start() {
        timer = new Timer("CHRONO", true);
        timer.schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                tick();
                mineSweeper.notifyAllObserversTimer();
            }
        }, 1000, 1000);
    }

    public void pause() {
        timer.cancel();
    }

    public void reset() {
        this.m = 0;
        this.s = 0;
    }

    private void tick() {
        ++s;
        m = m + s / 60;
        s %= 60;
    }

    public String toString() {
        String time = "";
        time += ((m < 10) ? "0" : "") + m + ":";
        time += ((s < 10) ? "0" : "") + s;

        return time;
    }

    public void stop() {
        pause();
    }
}
