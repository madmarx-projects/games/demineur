package main;

import controller.MineSweeperController;
import gui.MineSweeperUI;
import model.MineSweeper;
import utils.Settings;

public class Main {

    public static void main(String args[]) {

        new MineSweeperController(new MineSweeperUI(Settings.CASE_NUMBER_HEIGHT, Settings.CASE_NUMBER_WIDTH),
                new MineSweeper(Settings.CASE_NUMBER_HEIGHT, Settings.CASE_NUMBER_WIDTH));

    }
}
