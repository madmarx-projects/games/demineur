package state;

import model.Case;

public class FlaggedState implements State {

    private static FlaggedState instance = new FlaggedState();

    private FlaggedState() {}

    public static FlaggedState getInstance() {
        return instance;
    }

    @Override
    public boolean unveil(final Case c, final Case[][] landmine) {
        // ne rien faire, la case est bloquée par le flag
        return false;
    }

    @Override
    public int flag(final Case c) {
        c.setState(CoveredState.getInstance());

        return 1;
    }

    @Override
    public String getRepresentation(final Case c) {
        return "flag";
    }

    @Override
    public String getRepresentationFinishedGame(final Case c) {
        return "flagEndGame";
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
