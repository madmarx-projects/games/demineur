package state;

import model.Case;

public class CoveredState implements State {

    private static CoveredState instance = new CoveredState();

    private CoveredState() {}

    public static CoveredState getInstance() {
        return instance;
    }

    @Override
    public boolean unveil(final Case c, final Case[][] landmine) {
        c.setState(UncoveredState.getInstance());

        c.unveiled();

        try {
            if (c.getRepresentation().equals("mine") || Integer.valueOf(c.getRepresentation()) > 0) { return false; }
        }
        catch (final Exception e) {
            //
        }

        return true;
    }

    @Override
    public int flag(final Case c) {
        c.setState(FlaggedState.getInstance());

        return -1;
    }

    @Override
    public String getRepresentation(final Case c) {
        return "covered";
    }

    @Override
    public String getRepresentationFinishedGame(final Case c) {
        return null;
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }

}
