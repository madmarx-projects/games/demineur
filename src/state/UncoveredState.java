package state;

import model.Case;

public class UncoveredState implements State {

    private static UncoveredState instance = new UncoveredState();

    private UncoveredState() {}

    public static UncoveredState getInstance() {
        return instance;
    }

    @Override
    public boolean unveil(final Case c, final Case[][] landmine) {
        // ne rien faire, impossible de changer d'état quand la case est déjà dévoilée
        return false;
    }

    @Override
    public int flag(final Case c) {
        // ne rien faire, impossible de changer d'état quand la case est déjà dévoilée
        return 0;
    }

    @Override
    public String getRepresentation(final Case c) {
        return c.isMined() ? "mine" : String.valueOf(c.getProximityMines());
    }

    @Override
    public String getRepresentationFinishedGame(final Case c) {
        return c.isMined() ? "mineEndGame" : null;
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
