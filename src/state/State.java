package state;

import model.Case;
import model.MineSweeper;

import java.awt.event.MouseEvent;

public interface State {

    /**
     * Dévoiler la case
     * 
     * @param c
     *                 context
     * @param landmine
     * @return true si le state se dévoile false sinon
     */
    boolean unveil(Case c, Case[][] landmine);

    /**
     * Flagger la case
     * 
     * @param c
     *          context
     * @return valeur à additionner au nombre de mines restantes
     */
    int flag(Case c);

    /**
     * Obtient une représentation de l'état sous forme de String
     * 
     * @param c
     *          context
     * @return "mine" = mine, "flag" = flag, "0" = pas de mine à proximité, "1"
     *             = une mine à proximité, "2" = deux mine à proximité, etc
     */
    String getRepresentation(Case c);

    /**
     * Obtient une représentation de l'état sous forme de String
     * 
     * @param c
     *          context
     * @return FlaggedState : "flagEndGame", UncoveredState : "mineEndGame" si
     *             miné, null sinon, CoveredState : null,
     */
    String getRepresentationFinishedGame(Case c);
}
