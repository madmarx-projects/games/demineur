package model;

public interface IMineSweeper {

    /**
     * Retourne le nombres de mines générées dans cette partie
     * 
     * @return
     */
    int getNumberMines();

    /**
     * Gestion du clic par le model
     * 
     * @param lig
     * @param col
     * @param event
     */
    void click(int lig, int col, String event);

    /**
     * Additionne val au nombre de mines restantes.
     * 
     * @param val
     *            0, 1 ou -1
     */
    void addRemainingMines(int val);

    /**
     * Incrémente le nombre de cases dévoilées et check si la condition de
     * réussite est satisfaite ou non. Condition de réussite : la partie n'est
     * pas terminée et le nombre de mines dévoilées égale le nombre de case
     * soustrait du nombre de mines.
     */
    void caseUnveiled();

    /**
     * Demande au Chronometer de retourner le temps actuel.
     * 
     * @return
     */
    String getTime();

    /**
     * Avertit le model que la partie est terminée.
     * 
     * @param win
     *            si true, la partie est considérée gagnée, si false, la partie
     *            est considérée perdue.
     */
    void endGame(boolean win);

    /**
     * Retourne l'état de la partie.
     * 
     * @return true si la partie est bel et bien terminée, false sinon
     */
    boolean gameFinished();

    /**
     * Demande au model de regénérer un partie.
     */
    void restartGame();
}
