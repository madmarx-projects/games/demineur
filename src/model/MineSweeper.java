package model;

import controller.MineSweeperController;
import observer.Observable;
import utils.Chronometer;
import utils.Settings;

public class MineSweeper extends Observable implements IMineSweeper {

    private MineSweeperController controller;
    private final Chronometer     chronometer;

    private final int height;
    private final int width;

    private Case[][] landmine;
    private int      numberMines;
    private int      numberFlags;
    private int      unveiledCases;
    private boolean  endGame;

    public MineSweeper(int height, int width) {
        // forcer un minimum de cases
        if (height < 5) {
            height = 5;
        }
        if (width < 5) {
            width = 5;
        }

        this.height = height;
        this.width = width;

        this.chronometer = new Chronometer(this);
    }

    public void initLandmine() {
        this.endGame = false;
        this.numberMines = 0;
        this.numberFlags = 0;
        this.unveiledCases = 0;
        this.chronometer.reset();

        // creation
        System.out.println("CREATION OF A MINESWEEPER " + this.width + "x" + this.height);
        this.landmine = new Case[this.height][this.width];

        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                if (Math.random() * 50 <= Settings.DIFFICULTY_LVL) {
                    ++this.numberMines;
                    this.landmine[i][j] = new Case(i, j, true, this.controller);
                } else {
                    this.landmine[i][j] = new Case(i, j, false, this.controller);
                }
            }
        }

        // setting the numbers around the mines
        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {

                if (this.landmine[i][j].isMined()) {
                    for (var k = i - 1; k <= i + 1; k++) {
                        for (var l = j - 1; l <= j + 1; l++) {
                            try {
                                this.landmine[k][l].incProximityMines();
                            }
                            catch (final ArrayIndexOutOfBoundsException e) {

                            }
                        }
                    }
                }

            }
        }

        this.controller.initArrayUI(this.getNumberMines());
    }

    private void showLandmine() {
        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                String str;
                switch (this.landmine[i][j].getRepresentation()) {
                    case "covered":
                        str = Settings.COVERED;
                        break;

                    case "flag":
                        str = Settings.FLAGGED;
                        break;

                    case "mine":
                        str = Settings.MINED;
                        break;

                    default:
                        str = this.landmine[i][j].getRepresentation();
                        break;
                }

                System.out.print(str + " ");
            }
            System.out.println("");
        }
        System.out.println("\nNombre de mines : " + this.numberMines + "\n");
    }

    @Override
    public void click(final int lig, final int col, final String event) {
        switch (event) {
            case Settings.LEFT_CLICK_EVENT:
                this.landmine[lig][col].unveil(this.landmine);
                // System.out.println("=> " + this.unveiledCases + "/" + this.height * this.width);
                break;

            case Settings.RIGHT_CLICK_EVENT:
                this.landmine[lig][col].flag();
                break;
        }
    }

    @Override
    public void addRemainingMines(final int val) {
        this.numberMines += val;
        this.numberFlags += val;
        this.notifyRemainingMines(this.numberMines);
    }

    @Override
    public void caseUnveiled() {
        ++this.unveiledCases;

        if (!this.controller.gameFinished()
                && this.unveiledCases == (this.height * this.width - this.numberMines + this.numberFlags)) {
            // System.out.println("YOU WON !");
            this.controller.endGame(true);
        }
    }

    @Override
    public String getTime() {
        return this.chronometer.toString();
    }

    @Override
    public void endGame(final boolean win) {
        if (win) {
            this.unveilAllMines("coveredMineEndGame");
        } else {
            this.unveilAllMines("mineEndGame");
        }

        this.endGame = true;
        this.chronometer.stop();
    }

    @Override
    public boolean gameFinished() {
        return this.endGame;
    }

    @Override
    public void restartGame() {
        this.initLandmine();
    }

    @Override
    public int getNumberMines() {
        return this.numberMines;
    }

    private void unveilAllMines(final String res) {
        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                if (this.landmine[i][j].isMined()) {
                    this.controller.unveilUI(i, j, res);
                }
            }
        }
    }

    public void setController(final MineSweeperController controller) {
        this.controller = controller;
    }
}
