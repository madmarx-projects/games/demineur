package model;

import controller.MineSweeperController;
import state.CoveredState;
import state.FlaggedState;
import state.State;
import state.UncoveredState;

public class Case {

    private State   state;
    private int     proximityMines;
    private boolean mined;

    private int lig;
    private int col;

    private MineSweeperController controller;

    public Case(int lig, int col, boolean mined, MineSweeperController controller) {
        this.lig = lig;
        this.col = col;

        this.controller = controller;

        this.mined = mined;
        this.state = CoveredState.getInstance();
    }

    public String unveil(Case[][] landmine) {
        if (this.state.unveil(this, landmine)) {
            try {
                // en haut
                landmine[lig - 1][col].unveil(landmine);
            }
            catch (ArrayIndexOutOfBoundsException e) {}

            try {
                // à droite
                landmine[lig][col + 1].unveil(landmine);
            }
            catch (ArrayIndexOutOfBoundsException e) {}

            try {
                // en bas
                landmine[lig + 1][col].unveil(landmine);
            }
            catch (ArrayIndexOutOfBoundsException e) {}

            try {
                // à gauche
                landmine[lig][col - 1].unveil(landmine);
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }

        if (!controller.gameFinished()) {
            return this.state.getRepresentation(this);
        } else {
            return this.state.getRepresentationFinishedGame(this);
        }
    }

    public String flag() {
        controller.addRemainingMines(this.state.flag(this));

        controller.unveilUI(lig, col, this.getRepresentation());
        return this.getRepresentation();
    }

    public void unveiled() {
        controller.unveilUI(lig, col, this.getRepresentation());

        if (isMined() && !controller.gameFinished()) {
            System.err.println("YOU LOST " + lig + ";" + col);
            controller.endGame(false);
        }

        controller.caseUnveiled();
    }

    public void incProximityMines() {
        this.proximityMines++;
    }

    public boolean isMined() {
        return this.mined;
    }

    public String getRepresentation() {
        return state.getRepresentation(this);
    }

    public int getProximityMines() {
        return this.proximityMines;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String toString() {
        return "[" + lig + ";" + col + "]";
    }
}
