package controller;

import gui.IMineSweeperUI;
import gui.MineSweeperUI;
import model.IMineSweeper;
import model.MineSweeper;

import java.awt.event.MouseEvent;

public class MineSweeperController implements IMineSweeper, IMineSweeperUI {

    private IMineSweeperUI mineSweeperUI;
    private IMineSweeper   mineSweeper;

    public MineSweeperController(MineSweeperUI mineSweeperUI, MineSweeper mineSweeper) {
        this.mineSweeper = mineSweeper;
        mineSweeper.setController(this);

        this.mineSweeperUI = mineSweeperUI;
        mineSweeperUI.setController(this);

        mineSweeper.initLandmine();
        mineSweeper.attach(mineSweeperUI);
    }

    // MODEL --> VIEW
    @Override
    public void unveilUI(int lig, int col, String state) {
        mineSweeperUI.unveilUI(lig, col, state);
    }

    @Override
    public void initArrayUI(int nbMines) {
        mineSweeperUI.initArrayUI(mineSweeper.getNumberMines());
    }

    // VIEW --> MODEL
    @Override
    public int getNumberMines() {
        return mineSweeper.getNumberMines();
    }

    @Override
    public void click(int lig, int col, String event) {
        if (!gameFinished())
            mineSweeper.click(lig, col, event);
    }

    @Override
    public void addRemainingMines(int val) {
        mineSweeper.addRemainingMines(val);
    }

    @Override
    public String getTime() {
        return mineSweeper.getTime();
    }

    @Override
    public void endGame(boolean win) {
        mineSweeper.endGame(win);
        mineSweeperUI.endGame(win);
    }

    @Override
    public boolean gameFinished() {
        return mineSweeper.gameFinished();
    }

    @Override
    public void restartGame() {
        mineSweeper.restartGame();
    }

    // INTERNE
    @Override
    public void caseUnveiled() {
        mineSweeper.caseUnveiled();
    }

}
