#/bin/sh

shopt -s globstar

javac -d bin src/**/*.java

cd bin

jar cvfm Demineur.jar ../manifest.txt *

cd ..

mkdir -p build

mv bin/Demineur.jar ./build

